﻿using System;
using System.Text;
using _4xGameAI.Rules;
using _4xGameAI.Models;
using System.Collections.Generic;

namespace _4xGameAI
{
    class Program
    {
        static void Main(string[] args)
        {
            Player player = new Player() { 
                resources = new Dictionary<ResourceType, int>()
                {
                    { 
                        ResourceType.GOLD, 
                        1 
                    }
                }
            };

            Console.WriteLine("Player Has One Gold:");

            Console.WriteLine("Gold 1 tile away:");
            Console.WriteLine($"{(new GetResourceRule(ResourceType.GOLD)).GetScore(player, 1).Score}");

            Console.WriteLine("Gold 5 tile away:");
            Console.WriteLine($"{(new GetResourceRule(ResourceType.GOLD)).GetScore(player, 5).Score}");



            Console.WriteLine("Player Has Two Gold:");
            player.resources[ResourceType.GOLD] += 1;

            Console.WriteLine("Gold 1 tile away:");
            Console.WriteLine($"{(new GetResourceRule(ResourceType.GOLD)).GetScore(player, 1).Score}");

            Console.WriteLine("Gold 5 tile away:");
            Console.WriteLine($"{(new GetResourceRule(ResourceType.GOLD)).GetScore(player, 5).Score}");




        }
    }
}
