﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _4xGameAI
{
    public class RuleScoreModel
    {
        //This should be changed to whatever info you will need from the rule
        public string RuleName  { get; set; }
        public double Score { get; set; }
    }
}
