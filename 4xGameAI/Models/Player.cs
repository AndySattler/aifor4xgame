﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _4xGameAI.Models
{
    public enum ResourceType { GOLD }

    public class Player
    {
        public Dictionary<ResourceType, int> resources;
    }
}
