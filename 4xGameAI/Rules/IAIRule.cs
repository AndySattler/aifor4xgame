﻿using System;
using System.Collections.Generic;
using System.Text;
using _4xGameAI.Models;

namespace _4xGameAI.Rules
{
    interface IAIRule
    {
        RuleScoreModel GetScore(Player player, int distance);
    }
}
