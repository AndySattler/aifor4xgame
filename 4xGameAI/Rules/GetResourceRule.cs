﻿using _4xGameAI.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace _4xGameAI.Rules
{
    public class GetResourceRule : IAIRule
    {
        private readonly ResourceType _resource;

        public GetResourceRule(ResourceType resource)
        {
            _resource = resource;
        }

        public RuleScoreModel GetScore(Player player, int distance)
        {
            int resourceAmount = player.resources.Keys.Contains(_resource) ? player.resources[_resource] : 0;
            double resourceNeed = Math.Sqrt(-resourceAmount + 10) * 20;  // 0 is about 33
            double distanceModifier = Math.Sqrt(-distance + 20) * 10; //0 is about 22

            //I only have this have a max score of 55 so that other things have the possibility of being a higher priority
            double score = Math.Clamp(
                (Double.IsNaN(resourceNeed) ? 0 : resourceNeed) + (Double.IsNaN(distanceModifier) ? 0 : distanceModifier), 
                0, 
                100);

            return new RuleScoreModel()
            {
                RuleName = $"{_resource}Score",
                Score = score
            };

        }
    }
}
